package org.geektimes.projects.user.web.controller;

import org.geektimes.projects.user.domain.User;
import org.geektimes.projects.user.repository.DatabaseUserRepository;
import org.geektimes.projects.user.service.UserService;
import org.geektimes.projects.user.service.impl.UserServiceImpl;
import org.geektimes.projects.user.sql.DBConnectionManager;
import org.geektimes.web.mvc.controller.PageController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import java.sql.SQLException;
import java.util.Objects;


@Path("decide")
public class DecideController implements PageController {
    private UserService userService;
    {
        this.userService = new UserServiceImpl(new DatabaseUserRepository(new DBConnectionManager()));
    }
    @Override
    @Path("/success")
    @POST
    public String execute(HttpServletRequest request, HttpServletResponse response){
        try {
            User user = new User();
            user.setName(request.getParameter("name"));
            user.setPassword(request.getParameter("password"));
            user.setEmail(request.getParameter("email"));
            user.setPhoneNumber(request.getParameter("phoneNumber"));
            userService.register(user);
            System.out.println("注册成功");
            return "success.jsp";
        } catch (Exception e) {
            e.printStackTrace();
            request.setAttribute("error",e.getMessage());
            return "error.jsp";
        }
    }
}
