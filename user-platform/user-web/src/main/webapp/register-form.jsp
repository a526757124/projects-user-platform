<%--
  Created by IntelliJ IDEA.
  User: lijun
  Date: 2021/3/3
  Time: 21:08
  To change this template use File | Settings | File Templates.
--%>
<head>
    <jsp:directive.include file="/WEB-INF/jsp/prelude/include-head-meta.jspf" />
    <title>注 册</title>
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
</head>
<body>
<div class="container">
    <form action="${ pageContext.request.contextPath}/decide/success" method="post">
        <h1 class="h3 mb-3 font-weight-normal">注 册</h1>
        <label for="inputName" class="sr-only">用户登录名</label>
        <input type="name" id="inputName" class="form-control" placeholder="请输入登录名" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="请输入密码" required>

        <label for="inputEmail" class="sr-only">电子邮件</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="请输入电子邮件" required autofocus>
        <label for="inputPhone" class="sr-only">手机号</label>
        <input type="phoneNumber" id="inputPhone" class="form-control" placeholder="请输入手机号" required autofocus>

        <button class="btn btn-lg btn-primary btn-block" type="submit">提 交</button>
    </form>
</div>
</body>